const grpc = require("grpc");
const protoLoader = require("@grpc/proto-loader");
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');

const packageDef = protoLoader.loadSync("token.proto", {});
const grpcObject = grpc.loadPackageDefinition(packageDef);
const tokenPackage = grpcObject.tokenPackage;

/**
 * Start grpc server
 */

const server = new grpc.Server();
server.bind("0.0.0.0:4000", grpc.ServerCredentials.createInsecure());

server.addService(tokenPackage.TokenService.service, {
    "signToken": signTokenHandler,
    "verifyToken": verifyTokenHandler
});
server.start();
console.log('GRPC server listening on port 4000')

function signTokenHandler(call, callback) {
    const data = call.request.data
    const token = signToken(data)
    callback(null, { token });
}

function verifyTokenHandler(call, callback) {
    const token = call.request.token
    const data = verifyToken(token)
    callback(null, data)
}


/**
 * Start express server
 */

const express = require('express')
const app = express()

app.use(bodyParser.json())

app.post('/', function (req, res) {
    res.send(signToken(req.body.data))
})

app.get('/', function (req, res) {
    res.send(verifyToken(req.query.token))
})

app.listen(3000)
console.log('Rest server listening on port 3000')

function signToken(data) {
    const token = jwt.sign(data, 'shhhhh');
    return token
}

function verifyToken (token) {
    const decoded = jwt.verify(token, 'shhhhh');
    return decoded
}