const grpc = require("grpc");
const protoLoader = require("@grpc/proto-loader")
const Benchmark = require('benchmark');
const axios = require('axios')
const fetch = require('cross-fetch')

const packageDef = protoLoader.loadSync("token.proto", {});
const grpcObject = grpc.loadPackageDefinition(packageDef);
const tokenPackage = grpcObject.tokenPackage;

const client = new tokenPackage.TokenService("localhost:4000",
    grpc.credentials.createInsecure())

function signTokenGrpc (data) {
    return new Promise ((resolve, reject) => {
        client.signToken(data, (err, response) => {
            if (err) return reject(err)
            resolve(response)
        })
    })
}

function verifyTokenGrpc (token) {
    return new Promise ((resolve, reject) => {
        client.verifyToken(token, (err, response) => {
            if (err) return reject(err)
            resolve(response)
        })
    })
}

async function signTokenRest (data) {
    const res = await axios.post('http://localhost:3000', data)
    return res.data
}

async function verifyTokenRest (token) {
    const res = await axios.get(`http://localhost:3000?token=${token.token}`)
    return res.data
}

async function signTokenRestFetch (data) {
    const options = {
        method: 'POST',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify(data)
    }
    const res = await fetch('http://localhost:3000', options)
    return res.text()
}

async function verifyTokenRestFetch (token) {
    const res = await fetch(`http://localhost:3000?token=${token.token}`)
    return res.json()
}


// Benchmark

const suite = new Benchmark.Suite;
 
suite
.add('REST (Axios)      ', async () => {
    const data = {
        data: { username: "caio", password: "123" }
    }
    const token = await signTokenRest(data)
    await verifyTokenRest({ token })
})
.add('REST (cross-fetch)', async () => {
    const data = {
        data: { username: "caio", password: "123" }
    }
    const token = await signTokenRestFetch(data)
    await verifyTokenRestFetch({ token })
})
.add('gRPC              ', async () => {
    const data = {
        data: { username: "caio", password: "123" }
    }
    const { token } = await signTokenGrpc(data)
    await verifyTokenGrpc({ token })
})

// add listeners
.on('cycle', function(event) {
  console.log(String(event.target));
})
.on('complete', function() {
  console.log('Fastest is ' + this.filter('fastest').map('name'));
})
// run async
.run({
    'async': true,
});